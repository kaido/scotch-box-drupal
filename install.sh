#!/bin/bash

## Only thing you probably really care about is right here
DOMAINS=($*)

## Loop through all sites
for ((i=0; i < ${#DOMAINS[@]}; i++)); do

    ## Current Domain
    DOMAIN=${DOMAINS[$i]}

    echo "Creating directory for $DOMAIN..."
    mkdir -p /var/www/$DOMAIN/web

    echo "Creating vhost config for $DOMAIN..."
    sudo cp /etc/apache2/sites-available/scotchbox.local.conf /etc/apache2/sites-available/$DOMAIN.conf

    echo "Updating vhost config for $DOMAIN..."
    sudo sed -i s,scotchbox.local,$DOMAIN,g /etc/apache2/sites-available/$DOMAIN.conf
    sudo sed -i s,/var/www/public,/var/www/$DOMAIN/web,g /etc/apache2/sites-available/$DOMAIN.conf

    echo "Enabling $DOMAIN. Will probably tell you to restart Apache..."
    sudo a2ensite $DOMAIN.conf

    echo "So let's restart apache..."
    sudo service apache2 restart

done


#fix some issues with scotchbox
sudo add-apt-repository ppa:ondrej/php
# mysql -u root -proot scotchbox < /var/www/dump.sql
sudo apt-get update

# Disable obsolete PPA
echo "============================================"
echo "Disabling and deleting obsolete PPA."
echo "============================================"
sudo rm /etc/apt/sources.list.d/ondrej-php5-5_6-trusty.list
# Update to current PPA
echo "============================================"
echo "Adding current PPA and updating packages."
echo "============================================"
sudo add-apt-repository ppa:ondrej/php -y
sudo apt-get update

# Install PHP 7.1 Packages
echo "============================================"
echo "Installing PHP 7.1, packages, and apps."
echo "============================================"
sudo apt-get install php7.1 php7.1-common -y
sudo apt-get install php7-gd
sudo apt-get install php7.1-curl php7.1-xml php7.1-zip php7.1-gd php7.1-mysql php7.1-mbstring -y
sudo apt-get install php7.1-pgsql -y
sudo apt-get install php7.1-mcrypt -y
sudo apt-get install php7.1-fpm -y
sudo apt-get install php7.1-cgi -y
sudo apt-get install php7.1-intl -y
sudo apt-get install php7.1-sqlite3 -y
sudo apt-get install php-xdebug -y

#Update apache mods
sudo a2dismod php7.0
sudo a2enmod php7.1

# Configure Xdebug
echo "============================================"
echo "Configure Xdebug."
echo "============================================"
echo "; xdebug
zend_extension=xdebug.so
xdebug.remote_enable=1
xdebug.remote_connect_back=1
xdebug.show_exception_trace = 0
xdebug.collect_params = ?
xdebug.max_nesting_level = 256
xdebug.remote_port=9000
xdebug.remote_host=192.168.33.10" >> /etc/php/7.1/apache2/conf.d/20-xdebug.ini
# add composer to path
echo "============================================"

sudo sed -i s,'max_execution_time = 30','max_execution_time = 300',g /etc/php/7.1/apache2/php.ini
sudo sed -i s,'upload_max_filesize = 2M','upload_max_filesize = 1G',g /etc/php/7.1/apache2/php.ini
sudo sed -i s,'max_input_time = 60','max_input_time = 300',g /etc/php/7.1/apache2/php.ini
sudo sed -i s,'post_max_size = 8M','post_max_size = 128M',g /etc/php/7.1/apache2/php.ini
sudo sed -i s,'memory_limit = 128M','memory_limit = 1G',g /etc/php/7.1/apache2/php.ini
#update the composer
composer self-update

export PATH="~/.composer/vendor/bin:$PATH"
sudo service apache2 restart

# istall tools
# Drush launcher
wget -O drush.phar https://github.com/drush-ops/drush-launcher/releases/download/0.5.1/drush.phar
chmod +x drush.phar
sudo mv drush.phar /usr/local/bin/drush
drush self-update

#drupal console  launcher
curl https://drupalconsole.com/installer -L -o drupal.phar
mv drupal.phar /usr/local/bin/drupal
chmod +x /usr/local/bin/drupal
